INSERT INTO departemen (nama, id) VALUES ('Manajemen', 1);
INSERT INTO departemen (nama, id) VALUES ('Pengembangan Bisnis', 2);
INSERT INTO departemen (nama, id) VALUES ('Teknisi', 3);
INSERT INTO departemen (nama, id) VALUES ('Analisis', 4);

INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (1, 'Rizki Saputra', 'L', 'Menikah', to_date('10/11/1980', 'MM/DD/YYYY'), to_date('1/1/2011', 'MM/DD/YYYY'), 1);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (2, 'Farhan Reza', 'L', 'Belum', to_date('11/1/1989', 'MM/DD/YYYY'), to_date('1/1/2011', 'MM/DD/YYYY'), 1);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (3, 'Riyando Adi', 'L', 'Menikah', to_date('1/25/1977', 'MM/DD/YYYY'), to_date('1/1/2011', 'MM/DD/YYYY'), 1);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (4, 'Diego Manuel', 'L', 'Menikah', to_date('2/22/1983', 'MM/DD/YYYY'), to_date('9/4/2012', 'MM/DD/YYYY'), 2);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (5, 'Satya Laksana', 'L', 'Menikah', to_date('1/12/1981', 'MM/DD/YYYY'), to_date('3/19/2011', 'MM/DD/YYYY'), 2);

INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (6, 'Miguel Hernandez', 'L', 'Menikah', to_date('10/16/1994', 'MM/DD/YYYY'), to_date('6/15/2014', 'MM/DD/YYYY'), 2);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (7, 'Putri Persada', 'P', 'Menikah', to_date('1/30/1988', 'MM/DD/YYYY'), to_date('4/14/2013', 'MM/DD/YYYY'), 2);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (8, 'Alma Safira', 'P', 'Menikah', to_date('5/18/1991', 'MM/DD/YYYY'), to_date('9/8/2013', 'MM/DD/YYYY'), 3);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (9, 'Haqi Hafiz', 'L', 'Belum', to_date('9/19/1995', 'MM/DD/YYYY'), to_date('3/9/2015', 'MM/DD/YYYY'), 3);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (10, 'Abi Isyawara', 'L', 'Belum', to_date('6/3/1991', 'MM/DD/YYYY'), to_date('1/2/2013', 'MM/DD/YYYY'), 3);

INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (11, 'Maman Kresna', 'L', 'Belum', to_date('8/21/1993', 'MM/DD/YYYY'), to_date('9/15/2012', 'MM/DD/YYYY'), 3);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (12, 'Nadia Aulia', 'P', 'Belum', to_date('10/7/1989', 'MM/DD/YYYY'), to_date('5/7/2012', 'MM/DD/YYYY'), 4);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (13, 'Mutiara Rezki', 'P', 'Menikah', to_date('3/23/1988', 'MM/DD/YYYY'), to_date('5/21/2013', 'MM/DD/YYYY'), 4);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (14, 'Dani Setiawan', 'L', 'Belum', to_date('2/11/1986', 'MM/DD/YYYY'), to_date('11/30/2014', 'MM/DD/YYYY'), 4);
INSERT INTO karyawan (id, nama, "jenis_kelamin", status, "tanggal_lahir", "tanggal_masuk", departemen) VALUES (15, 'Budi Putra', 'L', 'Belum', to_date('10/23/1995', 'MM/DD/YYYY'), to_date('12/3/2015', 'MM/DD/YYYY'), 4);

