
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (1, 'Pak Budi', NULL, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (2, 'Pak Tono', 1, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (3, 'Pak Totok', 1, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (4, 'Bu Shinta', 2, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (5, 'Bu Novi', 3, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (6, 'Andre', 4, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (7, 'Dono', 4, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (8, 'Ismir', 5, 1);
INSERT INTO employee (id, nama, atasan_id, company_id) VALUES (9, 'Anto', 5, 1);

-- --  Mencari siapa CEO
SELECT * FROM employee WHERE atasan_id IS NULL;

-- -- Mencari Staff Biasa
SELECT * FROM employee WHERE atasan_id > 3;
--
-- -- Mencari Direktur
SELECT * FROM employee WHERE atasan_id = 1;
--
-- -- Mencari Manajer
SELECT * FROM employee WHERE atasan_id BETWEEN 2 AND 3;
--
-- -- Mencari jumlah bawahan dengan parameter nama
-- -- Bawahan Pak Budi berjumlah 8 orang
SELECT COUNT(atasan_id) as "Jumlah Bawahan Pak Budi" FROM employee;

-- -- Bawahan Bu Sinta berjumlah 2 orang
SELECT COUNT(*) AS Jumlah_Bawahan_Bu_Shinta
FROM employee
WHERE atasan_id = 4;



